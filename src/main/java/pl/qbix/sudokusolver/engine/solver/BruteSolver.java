
package pl.qbix.sudokusolver.engine.solver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.qbix.sudokusolver.engine.model.Board;
import pl.qbix.sudokusolver.engine.model.NoPossibilitiesLeftException;
import pl.qbix.sudokusolver.engine.model.Position;

public class BruteSolver {
    private static final Logger log = LoggerFactory.getLogger(BruteSolver.class);

    private Board masterBoard;
    private Board board;
    private Position position;

    public BruteSolver(Board board) throws NoPossibilitiesLeftException {
        this.masterBoard = new Board(board);
        this.board = new Board(masterBoard);
        this.position = findFirstNotFilledField(board);
    }

    public void run() throws NoPossibilitiesLeftException {
        if(board.isSolved()){
            log.info("Detected solved diagram. Ignoring");
        }else {
            setValue();
        }
    }

    public void setValue() throws NoPossibilitiesLeftException {
        boolean valueSet = false;
        while(!valueSet){
            int lowestPossibility = getLowestPossibility();
            try {
                log.debug("Trying to set field [{}][{}] value to {}", position.row, position.column, lowestPossibility);
                board.setValue(position, lowestPossibility);
                valueSet = true;
            }catch (NoPossibilitiesLeftException e){
                log.debug("Failed to set value");
                removePossibility(lowestPossibility);
            }
        }
    }

    private Integer getLowestPossibility() throws NoPossibilitiesLeftException {
        return board
                .getFieldPossibilities(position)
                .min()
                .getOrElseThrow(NoPossibilitiesLeftException::new);
    }

    private void removePossibility(int lowestPossibility) throws NoPossibilitiesLeftException {
        log.debug("removing possibility {} from field [{}][{}]", lowestPossibility, position.row, position.column);
        masterBoard.removeFieldPossibility(position, lowestPossibility);
        board = new Board(masterBoard);
    }

    private Position findFirstNotFilledField(Board board) {
        for(int rowNumber = 0; rowNumber < Board.BASE; rowNumber++){
            for(int columnNumber = 0; columnNumber < Board.BASE; columnNumber ++){
                if(board.getFieldValue(rowNumber, columnNumber) == null) {
                    return new Position(rowNumber, columnNumber);
                }
            }
        }
        throw new RuntimeException("Could not find any not filled field");
    }

    public Board getBoard(){
        return board;
    }

    public void deepSolve() throws NoPossibilitiesLeftException {
        while(!board.isSolved() && !board.getFieldPossibilities(position).isEmpty()){
            setValue();
            int currentValue = board.getFieldValue(position);
            if(board.isSolved()){
                break;
            }
            log.debug("Starting branch calculation for: \r\n{}" + board.getStringRepresentation());
            try {
                BruteSolver solver = new BruteSolver(board);
                solver.deepSolve();
                board = solver.getBoard();
            }catch (NoPossibilitiesLeftException e){
                removePossibility(currentValue);
            }
        }
    }
}
