package pl.qbix.sudokusolver;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;
import pl.qbix.sudokusolver.engine.model.Board;
import pl.qbix.sudokusolver.engine.model.NoPossibilitiesLeftException;
import pl.qbix.sudokusolver.engine.solver.BruteSolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SudokuSolver {
    public static void main(String[] args) throws InterruptedException, NoPossibilitiesLeftException {
        Board board = new Board();
        print(board);
        Logger root = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        try {
            while (true) {
                System.out.println("Enter row column value");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String command = bufferRead.readLine();
                if (command.equals("exit"))
                    System.exit(0);
                String[] params = command.split(" ");
                if(params[0].equals("pos")){
                    int row = Integer.parseInt(params[1]);
                    int column = Integer.parseInt(params[2]);
                    System.out.println(board.getFieldPossibilities(row,column));
                }
                else if(params[0].equals("posList")){
                    for(int rowNumber = 0; rowNumber< Board.BASE; rowNumber++)
                        for(int columnNumber=0; columnNumber<Board.BASE; columnNumber++)
                            if(board.getFieldValue(rowNumber,columnNumber) == null)
                                System.out.println("[" + rowNumber + "][" + columnNumber + "]: " + board.getFieldPossibilities(rowNumber,columnNumber));
                }
                else if(params[0].equals("deepSolve")){
                    BruteSolver solver = new BruteSolver(board);
                    solver.deepSolve();
                    board = solver.getBoard();
                    print(board);
                }
                else if(params[0].equals("init")){
                    board = new Board(params[1]);
                    print(board);
                }
                else{
                    int row = Integer.parseInt(params[0]);
                    int column = Integer.parseInt(params[1]);
                    int value = Integer.parseInt(params[2]);
                    board.setValue(row, column, value);
                    print(board);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void print(Board board) {
        System.out.println(board.getStringRepresentation());
    }
}
