package pl.qbix.sudokusolver.engine.model.generic;

import io.vavr.collection.HashSet;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.qbix.sudokusolver.engine.model.Position;

import static org.assertj.core.api.Assertions.assertThat;

public class BoardTest {
    @DataProvider(name = "bases")
    Object[][] bases() {
        return new Object[][]{
                {1}, {2}, {3}, {4}
        };
    }

    @Test(dataProvider = "bases")
    public void cleanBoardShouldHaveAllPossibilities(int base) {
        Board board = new Board(base);
        int maxValue = base * base;

        for (int i = 0; i < maxValue; i++) {
            for (int j = 0; j < maxValue; j++) {
                assertThat(board.getPossibilities(new Position(i, j))).isEqualTo(HashSet.rangeClosed(1, maxValue));
            }
        }
    }

    @Test
    public void setCorrectValue() {
        Board board = new Board(3);
        Position position = new Position(2, 2);

        board = board.setValue(position, 4);

        assertThat(board.getValue(position).get()).isEqualTo(4);
    }

    @Test
    public void setValueInFieldRemovesItFromRowPossibilities() {
        Board board = new Board(3);

        board = board.setValue(new Position(2, 0), 4);

        for (int i = 1; i < 9; i++) {
            assertThat(board.getPossibilities(new Position(2, i))).isEqualTo(HashSet.of(1, 2, 3, 5, 6, 7, 8, 9));
        }
    }

    @Test
    public void setValueInFieldRemovesItFromColumnPossibilities(){
        Board board = new Board(3);

        board = board.setValue(new Position(0, 2), 4);

        for (int i = 1; i < 9; i++) {
            assertThat(board.getPossibilities(new Position(i, 2))).isEqualTo(HashSet.of(1, 2, 3, 5, 6, 7, 8, 9));
        }
    }

    @Test
    public void setValueInFieldRemovesItFromLittleSquarePossibilities(){
        Board board = new Board(3);

        board = board.setValue(new Position(3,4), 4);

        for(int row = 3; row < 6; row++){
            for(int column = 3; column<6; column++){
                if(row !=3 || column !=4){
                    assertThat(board.getPossibilities(new Position(row, column))).isEqualTo(HashSet.of(1, 2, 3, 5, 6, 7, 8, 9));
                }
            }
        }
    }
}
