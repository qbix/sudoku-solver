package pl.qbix.sudokusolver.engine.model;

public class NoPossibilitiesLeftException extends InvalidFieldStateException {
    public NoPossibilitiesLeftException() {
        super();
    }
}
