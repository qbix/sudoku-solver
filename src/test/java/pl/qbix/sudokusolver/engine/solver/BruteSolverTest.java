package pl.qbix.sudokusolver.engine.solver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import pl.qbix.sudokusolver.engine.model.Board;
import pl.qbix.sudokusolver.engine.model.NoPossibilitiesLeftException;

import static org.assertj.core.api.Assertions.assertThat;

public class BruteSolverTest {

    public static final Logger log = LoggerFactory.getLogger(BruteSolverTest.class);

    @Test
    public void createdStoresDeepCopyOfBaseDiagram() throws NoPossibilitiesLeftException {
        String boardString = "123";
        Board board = new Board(boardString);
        BruteSolver solver = new BruteSolver(board);

        solver.run();

        log.info("\r\n" + board.getStringRepresentation());
        assertThat(board.getSimplifiedStringRepresentation()).isEqualTo(boardString);
    }

    @Test
    public void runFillsFirstEmptyFieldWithFirstPossibility() throws NoPossibilitiesLeftException {
        Board board = new Board("12");
        BruteSolver solver = new BruteSolver(board);

        solver.run();

        Board newBoard = solver.getBoard();
        log.info("\r\n" + board.getStringRepresentation());
        assertThat(newBoard.getFieldValue(0,2)).isEqualTo(3);
    }

    @Test
    public void runFillsFirstPossibilityThatDoesNotCrashDiagram() throws NoPossibilitiesLeftException {
        Board board = new Board("1234567894561");
        BruteSolver solver = new BruteSolver(board);

        solver.run();

        Board newBoard = solver.getBoard();
        log.info("\r\n" + newBoard.getStringRepresentation());
        assertThat(newBoard.getFieldValue(1,4)).isEqualTo(7);
    }

    @Test
    public void runDeepSolvesWholeSudoku() throws NoPossibilitiesLeftException {
        Board board = new Board();
        BruteSolver solver = new BruteSolver(board);
        String expectedBoard =
                "123456789" +
                "456789123" +
                "789123456" +
                "214365897" +
                "365897214" +
                "897214365" +
                "531642978" +
                "642978531" +
                "978531642";

        solver.deepSolve();

        log.info("\r\n" + solver.getBoard().getStringRepresentation());
        assertThat(solver.getBoard().getSimplifiedStringRepresentation()).isEqualTo(expectedBoard);
    }
}