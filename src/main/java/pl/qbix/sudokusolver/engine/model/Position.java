package pl.qbix.sudokusolver.engine.model;

public class Position {
    public int row;
    public int column;

    public Position(int row, int column){
        this.row = row;
        this.column = column;
    }

    public boolean equals(Position otherPosition){
        return (this.row == otherPosition.row) && (this.column == otherPosition.column);
    }
}
