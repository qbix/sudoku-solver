package pl.qbix.sudokusolver.engine.model;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BoardTest {
    @Test
    public void cleanBoardHasAllPossibilities() {
        Board board = new Board();

        for (int rowNumber = 0; rowNumber<9; rowNumber++)
            for (int columnNumber = 0; columnNumber<9; columnNumber++)
                assertThat(board.getFieldPossibilities(rowNumber, columnNumber)).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    @Test
    public void setValueOnFieldRemovesItFromPossibilitiesInRow() throws NoPossibilitiesLeftException {
        Board board = new Board();

        board.setValue(0, 0, 3);

        for (int columnNumber = 1; columnNumber < 9; columnNumber++) {
            assertThat(board.getFieldPossibilities(0,columnNumber)).containsExactly(1, 2, 4, 5, 6, 7, 8, 9);
        }
    }

    @Test
    public void setCorrectValue() throws NoPossibilitiesLeftException {
        Board board = new Board();

        board.setValue(0,0,3);

        assertThat(board.getFieldValue(0,0)).isEqualTo(3);
    }

    @Test
    public void setValueOnFieldRemovesItFromPossibilitiesInSmallSquare() throws NoPossibilitiesLeftException {
        Board board = new Board();

        board.setValue(0,0,3);

        for(int rowNumber = 0; rowNumber <3; rowNumber++)
            for(int columnNumber=0; columnNumber<3; columnNumber++)
                if(rowNumber != 0 || columnNumber != 0)
                    assertThat(board.getFieldPossibilities(rowNumber,columnNumber)).containsExactly(1,2,4,5,6,7,8,9);
    }

    @Test
    public void setValueOnFieldRemovesItFromPossibilitiesInColumn() throws NoPossibilitiesLeftException {
        Board board = new Board();

        board.setValue(0,0,3);

        for(int rowNumber = 1; rowNumber<9; rowNumber++) {
            assertThat(board.getFieldPossibilities(rowNumber,0)).containsExactly(1,2,4,5,6,7,8,9);
        }
    }

    @Test
    public void stringRepresentationReturnsASCIGraph() {
        Board board = new Board();
        int[][] values =
                {{1, 2, 3, 4, 5, 6, 7, 8, 9},
                        {4, 5, 6, 7, 8, 9, 1, 2, 3},
                        {7, 8, 9, 1, 2, 3, 4, 5, 6},
                        {2, 3, 4, 5, 6, 7, 8, 9, 1},
                        {5, 6, 7, 8, 9, 1, 2, 3, 4},
                        {8, 9, 1, 2, 3, 4, 5, 6, 7},
                        {3, 4, 5, 6, 7, 8, 9, 1, 2},
                        {6, 7, 8, 9, 1, 2, 3, 4, 5},
                        {9, 1, 2, 3, 4, 5, 6, 7, 8,}
                };
        String expectedString =
                "*-------*-------*-------*\r\n" +
                "| 1 2 3 | 4 5 6 | 7 8 9 |\r\n" +
                "| 4 5 6 | 7 8 9 | 1 2 3 |\r\n" +
                "| 7 8 9 | 1 2 3 | 4 5 6 |\r\n" +
                "*-------*-------*-------*\r\n" +
                "| 2 3 4 | 5 6 7 | 8 9 1 |\r\n" +
                "| 5 6 7 | 8 9 1 | 2 3 4 |\r\n" +
                "| 8 9 1 | 2 3 4 | 5 6 7 |\r\n" +
                "*-------*-------*-------*\r\n" +
                "| 3 4 5 | 6 7 8 | 9 1 2 |\r\n" +
                "| 6 7 8 | 9 1 2 | 3 4 5 |\r\n" +
                "| 9 1 2 | 3 4 5 | 6 7 8 |\r\n" +
                "*-------*-------*-------*";

        board.setValues(values);

        assertThat(board.getStringRepresentation()).isEqualTo(expectedString);
    }

    @Test
    public void stringRepresentationOnEmptyFieldsKeepsAlignment(){
        Board board = new Board();
        String expectedString =
                        "*-------*-------*-------*\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "*-------*-------*-------*\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "*-------*-------*-------*\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "|       |       |       |\r\n" +
                        "*-------*-------*-------*";
        assertThat(board.getStringRepresentation()).isEqualTo(expectedString);
    }

    @Test
    public void shouldPropagatePossibilitiesIfValueIsSetDueToPossibilityRemoval() throws NoPossibilitiesLeftException {
        Board board = new Board();
        String expectedString =
                "*-------*-------*-------*\r\n" +
                "| 6 1 2 | 8 5 3 | 4 7 9 |\r\n" +
                "| 7 8 4 | 1 9 6 | 2 5 3 |\r\n" +
                "| 9 3 5 |       |       |\r\n" +
                "*-------*-------*-------*\r\n" +
                "|       |       |       |\r\n" +
                "|       |       |       |\r\n" +
                "|       |       |       |\r\n" +
                "*-------*-------*-------*\r\n" +
                "|       |       |       |\r\n" +
                "|       |       |       |\r\n" +
                "|       |       |       |\r\n" +
                "*-------*-------*-------*";

        board.setValue(0,0,6);
        board.setValue(0,1,1);
        board.setValue(0,3,8);
        board.setValue(0,4,5);
        board.setValue(0,5,3);
        board.setValue(0,6,4);
        board.setValue(0,8,9);
        board.setValue(1,0,7);
        board.setValue(1,1,8);
        board.setValue(1,2,4);
        board.setValue(1,3,1);
        board.setValue(1,4,9);
        board.setValue(1,5,6);
        board.setValue(1,7,5);
        board.setValue(1,8,3);
        board.setValue(2,0,9);
        board.setValue(2,1,3);
        board.setValue(2,2,5);


        assertThat(board.getStringRepresentation()).isEqualTo(expectedString);
    }

    @Test
    public void parsingBoardFromStringFillsFieldFromLeftToRightThenTopToBottom() throws NoPossibilitiesLeftException {
        String boardString = "123456789456";

        Board board = new Board(boardString);

        assertThat(board.getFieldValue(0,0)).isEqualTo(1);
        assertThat(board.getFieldValue(0,1)).isEqualTo(2);
        assertThat(board.getFieldValue(0,2)).isEqualTo(3);
        assertThat(board.getFieldValue(0,3)).isEqualTo(4);
        assertThat(board.getFieldPossibilities(1,3)).doesNotContain(4,5,6);
    }

    @Test
    public void parsingBoardTreatsDotAsEmptyField() throws NoPossibilitiesLeftException {
        String boardString = "1.3";

        Board board = new Board(boardString);

        assertThat(board.getFieldValue(0,0)).isEqualTo(1);
        assertThat(board.getFieldValue(0,1)).isNull();
        assertThat(board.getFieldValue(0,2)).isEqualTo(3);
    }

    @Test
    public void parsingBoardSkipsAllNonDigitOrDotCharacters() throws NoPossibilitiesLeftException {
        String boardString = "12!@#$%^&*()_+asdf,\r\n3";

        Board board = new Board(boardString);

        assertThat(board.getFieldValue(0,0)).isEqualTo(1);
        assertThat(board.getFieldValue(0,1)).isEqualTo(2);
        assertThat(board.getFieldValue(0,2)).isEqualTo(3);
    }

    @Test
    public void simplifiedStringIsReverseToParsing() throws NoPossibilitiesLeftException {
        String boardString = "1...567894.5.7";
        Board board = new Board(boardString);

        String simplifiedString = board.getSimplifiedStringRepresentation();

        assertThat(simplifiedString).isEqualTo(boardString);
    }

    @Test
    public void cleanBoardIsNotSolved(){
        Board board = new Board();

        assertThat(board.isSolved()).isFalse();
    }

    @Test
    public void fullyFilledBoardIsSolved(){
        Board board = new Board();
        int[][] values =
                {{1, 2, 3, 4, 5, 6, 7, 8, 9},
                        {4, 5, 6, 7, 8, 9, 1, 2, 3},
                        {7, 8, 9, 1, 2, 3, 4, 5, 6},
                        {2, 3, 4, 5, 6, 7, 8, 9, 1},
                        {5, 6, 7, 8, 9, 1, 2, 3, 4},
                        {8, 9, 1, 2, 3, 4, 5, 6, 7},
                        {3, 4, 5, 6, 7, 8, 9, 1, 2},
                        {6, 7, 8, 9, 1, 2, 3, 4, 5},
                        {9, 1, 2, 3, 4, 5, 6, 7, 8}
                };
        board.setValues(values);

        assertThat(board.isSolved()).isTrue();
    }

    @Test
    public void DeepCopyKeepsInformationOnPossibilities() throws NoPossibilitiesLeftException {
        Board board = new Board();
        board.removeFieldPossibility(0,0,5);

        Board secondBoard = new Board(board);

        assertThat(secondBoard.getFieldPossibilities(0,0)).doesNotContain(5);
    }
}