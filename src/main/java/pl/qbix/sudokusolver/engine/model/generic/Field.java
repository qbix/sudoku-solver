package pl.qbix.sudokusolver.engine.model.generic;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import pl.qbix.sudokusolver.engine.model.ImpossibleValueSetException;
import pl.qbix.sudokusolver.engine.model.NoPossibilitiesLeftException;

import static io.vavr.control.Option.none;

public class Field {
    private final Set<Integer> possibilities;
    private final Option<Integer> value;

    public Field(int value) {
        this(HashSet.of(value), Option.of(value));
    }

    public Field(Set<Integer> possibilities) {
        this(possibilities, none());
    }

    private Field(Set<Integer> possibilities, Option<Integer> value) {
        if (possibilities.isEmpty()) {
            throw new NoPossibilitiesLeftException();
        }
        if (!value.isEmpty()) {
            possibilities = setPossibilities(possibilities, value);
        }
        this.possibilities = possibilities;
        this.value = value;
    }

    private Set<Integer> setPossibilities(Set<Integer> possibilities, Option<Integer> value) {
        if (!possibilities.contains(value.get())) {
            throw new ImpossibleValueSetException(possibilities, value.get());
        }else{
            possibilities = HashSet.of(value.get());
        }
        return possibilities;
    }

    public Set<Integer> getPossibilities() {
        return possibilities;
    }

    public Field removePossiblity(int possibility) {
        Set<Integer> newPos = possibilities.remove(possibility);
        return new Field(newPos, value);
    }

    public Field setValue(int value) {
        return new Field(possibilities, Option.of(value));
    }

    public Option<Integer> getValue() {
        return value;
    }

    public static Field fieldForBase(int base) {
        return new Field(HashSet.rangeClosed(1, base * base), none());
    }
}
