package pl.qbix.sudokusolver.engine.model;

import io.vavr.collection.Set;
import org.apache.commons.lang3.StringUtils;
import pl.qbix.sudokusolver.engine.model.generic.Field;
import org.slf4j.LoggerFactory;

import static pl.qbix.sudokusolver.engine.model.generic.Field.fieldForBase;

public class Board {
    private static final String ROW_SEPARATOR = "*-------*-------*-------*";
    public static final int BASE = 9;
    private final Field[][] fields;

    public Board() {
        fields = new Field[BASE][BASE];
        for (int i = 0; i < BASE; i++)
            for (int j = 0; j < BASE; j++)
                fields[i][j] = fieldForBase(3);
    }

    public Board(String boardString) throws NoPossibilitiesLeftException {
        this();
        String[] characters = boardString.split("");
        int column = 0;
        int row = 0;
        for(String character : characters){
            if(!character.matches("\\.|\\d"))
                continue;
            if(!character.equals(".")) {
                setValue(row, column, Integer.parseInt(character));
            }
            column++;
            if(column >= BASE){
                column = 0;
                row++;
            }
        }
    }

    public Board(Board board) {
        this();
        for(int rowNumber = 0; rowNumber<BASE; rowNumber++){
            for(int columnNumber = 0; columnNumber<BASE; columnNumber++){
                this.fields[rowNumber][columnNumber] = board.fields[rowNumber][columnNumber];
            }
        }
    }

    private Field[][] getFields() {
        return fields;
    }

    public Integer getFieldValue(int row, int column){
        return fields[row][column].getValue().getOrNull();
    }

    public Set<Integer> getFieldPossibilities(int row, int column){
        return fields[row][column].getPossibilities();
    }

    public void setValue(int row, int column, int value) throws NoPossibilitiesLeftException {
        fields[row][column] = fields[row][column].setValue(value);
        removePossibilitiesFromOtherFields(row, column, value);
    }

    private void removePossibilitiesFromOtherFields(int row, int column, int value) throws NoPossibilitiesLeftException {
        removePossibilitiesFromRow(row, column, value);
        removePossibilitiesFromColumn(row, column, value);
        removePossibilitiesFromSmallSquare(row, column, value);
    }

    private void removePossibilitiesFromSmallSquare(int row, int column, int value) throws NoPossibilitiesLeftException {
        int startingRow = (row / 3) * 3;
        int startingColumn = (column / 3) * 3;
        for (int i = startingRow; i < startingRow + 3; i++) {
            for (int j = startingColumn; j < startingColumn + 3; j++) {
                if (i != row || j != column)
                    removeFieldPossibility(i, j, value);
            }
        }
    }

    private void removePossibilitiesFromColumn(int row, int column, int value) throws NoPossibilitiesLeftException {
        for (int i = 0; i < BASE; i++)
            if (i != row)
                removeFieldPossibility(i, column, value);
    }

    private void removePossibilitiesFromRow(int row, int column, int value) throws NoPossibilitiesLeftException {
        for (int i = 0; i < BASE; i++)
            if (i != column)
                removeFieldPossibility(row, i, value);
    }

    public void removeFieldPossibility(int row, int column, int value) throws NoPossibilitiesLeftException {
        Field field = fields[row][column];
        boolean wasEmpty = field.getValue().isEmpty();
        try {
            fields[row][column] = field.removePossiblity(value);
            if(fields[row][column].getPossibilities().size() == 1){
                fields[row][column] = fields[row][column].setValue(fields[row][column].getPossibilities().get());
            }
        }catch(NoPossibilitiesLeftException e){
            LoggerFactory.getLogger(Board.class).debug("No possibilities left on field [{}][{}] after removing option {}", row, column, value);
            throw e;
        }
        if (wasEmpty && fields[row][column].getValue().isDefined()) {
            LoggerFactory.getLogger(Board.class).debug("Value of [{}][{}] set by deduction to {}", row, column, field.getValue());
            removePossibilitiesFromOtherFields(row, column, fields[row][column].getValue().get());
        }
    }

    public void setValues(int[][] values) {
        for (int i = 0; i < values.length; i++)
            for (int j = 0; j < values[i].length; j++) {
                Field field = new Field(values[i][j]);
                fields[i][j] = field;
            }
    }

    public String getStringRepresentation() {
        String table = ROW_SEPARATOR + "\r\n";
        for (int i = 0; i < fields.length; i++) {
            table += getRowString(fields[i]);
            if (i % 3 == 2 && i != fields.length - 1)
                table += ROW_SEPARATOR + "\r\n";
        }
        table += ROW_SEPARATOR;
        return table;
    }

    private String getRowString(Field[] fields) {
        String rowString = "|";
        for (int i = 0; i < fields.length; i++) {
            String fieldString = " ";
            Integer value = fields[i].getValue().getOrNull();
            if (value != null)
                fieldString = value.toString();
            rowString += " " + fieldString;
            if (i % 3 == 2)
                rowString += " |";
        }
        rowString += "\r\n";
        return rowString;
    }

    public boolean isSolved() {
        for (Field[] row : fields)
            for (Field field : row)
                if (field.getPossibilities().size() != 1)
                    return false;
        return true;
    }

    public String getSimplifiedStringRepresentation() {
        StringBuilder builder = new StringBuilder();
        for(Field[] row : getFields()){
            for(Field field: row){
                if(field.getValue().isEmpty()){
                    builder.append(".");
                }else{
                    builder.append(field.getValue().get());
                }
            }
        }
        return StringUtils.strip(builder.toString(), ".");
    }

    public Set<Integer> getFieldPossibilities(Position position) {
        return getFieldPossibilities(position.row, position.column);
    }

    public void setValue(Position position, int lowestPossibility) throws NoPossibilitiesLeftException {
        setValue(position.row, position.column, lowestPossibility);
    }

    public void removeFieldPossibility(Position position, int value) throws NoPossibilitiesLeftException {
        removeFieldPossibility(position.row, position.column, value);
    }

    public int getFieldValue(Position position) {
        return getFieldValue(position.row, position.column);
    }
}
