package pl.qbix.sudokusolver.engine.model;

public class InvalidFieldStateException extends RuntimeException {
    public InvalidFieldStateException(String message) {
        super(message);
    }

    public InvalidFieldStateException(){
        super();
    }
}
