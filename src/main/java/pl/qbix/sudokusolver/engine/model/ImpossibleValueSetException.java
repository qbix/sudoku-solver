package pl.qbix.sudokusolver.engine.model;

import io.vavr.collection.Set;

public class ImpossibleValueSetException extends InvalidFieldStateException {
    public ImpossibleValueSetException(Set<Integer> possibilities, int value) {
        super(String.format("Trying to set value of %1$d, where only %2$s are available", value, possibilities));
    }
}
