package pl.qbix.sudokusolver.engine.model.generic;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.qbix.sudokusolver.engine.model.ImpossibleValueSetException;
import pl.qbix.sudokusolver.engine.model.NoPossibilitiesLeftException;

import static io.vavr.control.Option.none;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.qbix.sudokusolver.engine.model.generic.Field.fieldForBase;

public class FieldTest {
    @DataProvider (name = "possibilities")
    Object[][] possibilities(){
        return new Object[][]{
                {1, HashSet.of(1)},
                {2, HashSet.of(1,2,3,4)},
                {3, HashSet.of(1,2,3,4,5,6,7,8,9)}
        };
    }

    @Test(dataProvider = "possibilities")
    public void shouldHaveAllPossibilities(int base, Set<Integer> expectedPossibilities){
        Field field = fieldForBase(base);

        Set<Integer> possibilities = field.getPossibilities();

        assertThat(possibilities).isEqualTo(expectedPossibilities);
    }

    @Test
    public void shouldHavAllPossibilitiesFromInput(){
        Set<Integer> possibilities = HashSet.of(1,2,3,5,6);

        Field field = new Field(possibilities);

        assertThat(field.getPossibilities()).isEqualTo(possibilities);
    }

    @Test
    public void shouldReturnLessPossibilitiesOnRemoval(){
        Field field = fieldForBase(3);

        field = field.removePossiblity(2);

        assertThat(field.getPossibilities()).isEqualTo(HashSet.of(1,3,4,5,6,7,8,9));
    }

    @Test(expectedExceptions = NoPossibilitiesLeftException.class)
    public void shouldThrowExceptionOnLastPossibilityRemoved(){
        Field field = new Field(HashSet.of(1));

        field.removePossiblity(1);
    }

    @Test
    public void shouldNarrowPossibilitiesToValueSet(){
        Field field = new Field(HashSet.of(1,2,3));

        field = field.setValue(2);

        assertThat(field.getPossibilities()).isEqualTo(HashSet.of(2));
    }

    @Test(expectedExceptions = ImpossibleValueSetException.class)
    public void shouldThrowExceptionOnSettingNotPossibleValue(){
        Field field = new Field(HashSet.of(1,2,3));

        field.setValue(4);
    }

    @Test
    public void shouldNotSetValueEvenOnLastPossibility(){
        Field field = new Field(HashSet.of(1,2));

        field = field.removePossiblity(1);

        assertThat(field.getValue()).isEqualTo(none());
    }

    @Test
    public void shouldContainValue(){
        Field field = new Field(HashSet.of(1,2));

        field = field.setValue(1);

        assertThat(field.getValue()).isEqualTo(Option.of(1));
    }

    @Test
    public void shouldCreateFieldWithCorrectValue(){
        Field field = new Field(3);

        assertThat(field.getValue().get()).isEqualTo(3);
        assertThat(field.getPossibilities()).isEqualTo(HashSet.of(3));
    }
}