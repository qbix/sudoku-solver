package pl.qbix.sudokusolver.engine.model.generic.board;

public class Range {
    private final int firstRow;
    private final int lastRow;
    private final int firstColumn;
    private final int lastColumn;

    public Range(int firstRow, int lastRow, int firstColumn, int lastColumn) {
        this.firstRow = firstRow;
        this.lastRow = lastRow;
        this.firstColumn = firstColumn;
        this.lastColumn = lastColumn;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public int getLastRow() {
        return lastRow;
    }

    public int getFirstColumn() {
        return firstColumn;
    }

    public int getLastColumn() {
        return lastColumn;
    }
}
