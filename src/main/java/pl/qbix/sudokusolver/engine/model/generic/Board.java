package pl.qbix.sudokusolver.engine.model.generic;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import pl.qbix.sudokusolver.engine.model.Position;
import pl.qbix.sudokusolver.engine.model.generic.board.Range;

import static pl.qbix.sudokusolver.engine.model.generic.Field.fieldForBase;

public class Board {
    private final int maxValue;
    private final int base;
    private final Field[][] fields;

    public Board(int base) {
        this.base = base;
        this.maxValue = base * base;
        fields = getCleanFields(base);
    }

    private Board(Field[][] fields) {
        this.maxValue = fields.length;
        this.base = (int) Math.sqrt(maxValue);
        this.fields = fields;
    }

    private static Field[][] getCleanFields(int base) {
        int maxValue = base * base;
        Field[][] fields = new Field[maxValue][maxValue];
        for (int i = 0; i < maxValue; i++) {
            for (int j = 0; j < maxValue; j++) {
                fields[i][j] = fieldForBase(base);
            }
        }
        return fields;
    }

    public Set<Integer> getPossibilities(Position position) {
        return fields[position.row][position.column].getPossibilities();
    }

    public Board setValue(Position position, int value) {
        Field[][] fields = copyFields(this.fields);
        fields[position.row][position.column] = fields[position.row][position.column].setValue(value);
        Board board = new Board(fields);
        board = board.removePossibilityFromOtherFields(position, value);
        return board;
    }

    private Board removePossibilityFromOtherFields(Position position, int value) {
        Board board = this;
        board = board.removePossibilityFromRow(position, value);
        board = board.removePossibilityFromColumn(position, value);
        board = board.removePossibilityFromLittleSquare(position, value);
        return board;
    }

    private Board removePossibilityFromRow(Position position, int value) {
        Board board = this;
        Range rowRange = new Range(position.row, position.row, 0, maxValue-1);
        board = board.removePossibilityFromRange(position, rowRange, value);
        return board;
    }

    private Board removePossibilityFromColumn(Position position, int value) {
        Board board = this;
        Range columnRange = new Range(0, maxValue - 1, position.column, position.column);
        board = board.removePossibilityFromRange(position, columnRange, value);
        return board;
    }

    private Board removePossibilityFromLittleSquare(Position position, int value) {
        Board board = this;
        int firstRow = (position.row / base) * base;
        int lastRow = firstRow + (base - 1);
        int firstColumn = (position.column / base) * base;
        int lastColumn = firstColumn + (base - 1);
        Range range =  new Range(firstRow, lastRow, firstColumn, lastColumn);
        board = board.removePossibilityFromRange(position, range, value);
        return board;
    }

    private Board removePossibilityFromRange(Position position, Range range, int value) {
        Board board = this;
        for (int row = range.getFirstRow(); row <= range.getLastRow(); row++) {
            for (int column = range.getFirstColumn(); column <= range.getLastColumn(); column++) {
                Position pos = new Position(row, column);
                if (!pos.equals(position)) {
                    board = board.removePossibility(new Position(row, column), value);
                }
            }
        }
        return board;
    }

    private Board removePossibility(Position position, int value) {
        Field[][] fields = copyFields(this.fields);
        fields[position.row][position.column] = fields[position.row][position.column].removePossiblity(value);
        return new Board(fields);
    }

    private static Field[][] copyFields(Field[][] fields) {
        Field[][] newFields = new Field[fields.length][];
        for (int i = 0; i < fields.length; i++) {
            Field[] columnFields = new Field[fields[i].length];
            for (int j = 0; j < fields[i].length; j++) {
                columnFields[j] = fields[i][j];
            }
            newFields[i] = columnFields;
        }
        return newFields;
    }

    public Option<Integer> getValue(Position position) {
        return fields[position.row][position.column].getValue();
    }
}
